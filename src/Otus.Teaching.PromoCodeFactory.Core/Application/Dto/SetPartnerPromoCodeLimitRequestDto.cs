﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Application.Dto
{
    public class SetPartnerPromoCodeLimitRequestDto
    {
        public DateTime EndDate { get; set; }
        public int Limit { get; set; }
    }
}