﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Gateways;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Application.Services
{
    public class PartnerService
        : IPartnerService
    {
        private readonly IRepository<Partner> _partnersRepository;
        private readonly INotificationGateway _notificationGateway;
        private readonly ICurrentDateTimeProvider _currentDateTimeProvider;

        public PartnerService(IRepository<Partner> partnersRepository, INotificationGateway notificationGateway,
            ICurrentDateTimeProvider currentDateTimeProvider)
        {
            _partnersRepository = partnersRepository;
            _notificationGateway = notificationGateway;
            _currentDateTimeProvider = currentDateTimeProvider;
        }
        
        public async Task<List<PartnerResponseDto>> GetPartnersAsync()
        {
            var partners = await _partnersRepository.GetAllAsync();

            var response = partners.Select(x => new PartnerResponseDto()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponseDto()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            }).ToList();

            return response;
        }

        public async Task<PartnerResponseDto> GetPartnerByIdAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            var response = new PartnerResponseDto()
            {
                Id = partner.Id,
                Name = partner.Name,
                NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = partner.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponseDto()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            };

            return response;
        }

        public async Task<PartnerPromoCodeLimitResponseDto> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                throw new EntityNotFoundException();
            
            var limit = partner.PartnerLimits
                .FirstOrDefault(x => x.Id == limitId);

            var response = new PartnerPromoCodeLimitResponseDto()
            {
                Id = limit.Id,
                PartnerId = limit.PartnerId,
                Limit = limit.Limit,
                CreateDate = limit.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                EndDate = limit.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                CancelDate = limit.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
            };

            return response;
        }

        public async Task<Guid> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDto requestDto)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                throw new EntityNotFoundException();
            
            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new ChangePartnerLimitException("Данный партнер не активен");
            
            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;
                
                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }

            if (requestDto.Limit <= 0)
                throw new ChangePartnerLimitException("Лимит должен быть больше 0");
            
            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = requestDto.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = _currentDateTimeProvider.CurrentDateTime,
                EndDate = requestDto.EndDate
            };
            
            partner.PartnerLimits.Add(newLimit);

            await _partnersRepository.UpdateAsync(partner);
            
            await _notificationGateway
                .SendNotificationToPartnerAsync(partner.Id, "Вам установлен лимит на отправку промокодов...");

            return newLimit.Id;
        }

        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);
            
            if (partner == null)
                throw new EntityNotFoundException();
            
            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new ChangePartnerLimitException("Данный партнер не активен");
            
            //Отключение лимита
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                activeLimit.CancelDate = _currentDateTimeProvider.CurrentDateTime;
            }

            await _partnersRepository.UpdateAsync(partner);

            //Отправляем уведомление
            await _notificationGateway
                .SendNotificationToPartnerAsync(partner.Id, "Ваш лимит на отправку промокодов отменен...");
        }
    }
}